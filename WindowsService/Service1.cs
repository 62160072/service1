﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net.Http.Headers;
using System.Net.Http;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;
using System.Net.NetworkInformation;
using System.Net;
using System.Management;
using System.ServiceProcess;
using System.Configuration.Install;
using System.Threading;
using System.Runtime.InteropServices;
using static WindowsService.Service1;
using System.Net.Sockets;

namespace WindowsService
{
    [RunInstaller(true)]
    public partial class Service1 : ServiceBase
    {
        public Service1()
        {
            InitializeComponent();
        
        }


        public class Mac
        {
            //public int id { get; set; }
            public string ip { get; set; }
            public string mac { get; set; }
            public string timeOpen { get; set; }
            public string lastTimeShutdown { get; set; }
            public string cpu { get; set; }
            public string mainboard { get; set; }
            public string ram { get; set; }

        }

        public class Time_info
        {
            //public int id { get; set; }
            public string ip { get; set; }
            public string year { get; set; }
            public string month { get; set; }
            public string day { get; set; }
            public string hour { get; set; }
            public string shutdownHour { get; set; }
            public string shutdownMinute { get; set; }
            public string minute { get; set; }
            public string second { get; set; }
            

        }

        public string addmac;
        public string addtime;
        public string addip;
        public string addlasttime;
        public string addcpu;
        public string addmainbord;
        public string addram;


        public string addyear;
        public string addmouth;
        public string addday;
        public string addhour;
        public string addshutdownH;
        public string addshutdownM;
        public string addminute;
        public string addsecond;
        

        public int milliseconds = 20000;

        public bool checkUpdate = false;

        Thread thread;


        static HttpClient client = new HttpClient();

        static async Task<Uri> CreateProductAsync(Mac mac, Time_info time_info)
        {
            

            HttpResponseMessage response = await client.PostAsJsonAsync(
                "/mac", mac);
            HttpResponseMessage response2 = await client.PostAsJsonAsync(
                "/time-info", time_info);
            response.EnsureSuccessStatusCode();
            response2.EnsureSuccessStatusCode();
            


            

            // return URI of the created resource.
            return response.Headers.Location;
        }

        static async Task<Mac> UpdateProductAsync(Mac mac, Time_info time_info)
        {
            HttpResponseMessage response = await client.PutAsJsonAsync(
                $"/mac/update/{mac.timeOpen}", mac);

            HttpResponseMessage response2 = await client.PutAsJsonAsync(
                $"/time-info/update/{time_info.year}/{time_info.month}/{time_info.day}/{time_info.hour}/{time_info.minute}/{time_info.second}", time_info);

            response.EnsureSuccessStatusCode();
            response2.EnsureSuccessStatusCode();

            mac = await response.Content.ReadAsAsync<Mac>();
            return mac;


        }

        

        public async void RunAsync()
        {

            // Update port # in the following line.
            Service1 myService = new Service1();
            client.BaseAddress = new Uri("http://playground.informatics.buu.ac.th:8999/");
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(
                new MediaTypeWithQualityHeaderValue("application/json"));


            try
            {
                // Create a new product

                Mac mac = new Mac
                {
                    ip = addip,
                    mac = addmac,
                    timeOpen = addtime,
                    lastTimeShutdown = addlasttime,
                    cpu = addcpu,
                    mainboard = addmainbord,
                    ram = addram,

                };

                Time_info time_info = new Time_info
                {
                    ip = addip,
                    year = addyear,
                    month = addmouth,
                    day = addday,
                    hour = addhour,
                    shutdownHour = addshutdownH,
                    minute = addminute,
                    shutdownMinute = addshutdownM,
                    second = addsecond,
                    
                };


                    var url = await CreateProductAsync(mac, time_info);
                    Console.WriteLine($"Created at {url}");
                    
                
                while (true)
                {
                    this.GetLastTime(null);
                    mac.lastTimeShutdown = addlasttime;
                    time_info.shutdownHour = addshutdownH;
                    time_info.shutdownMinute = addshutdownM;
                    await UpdateProductAsync(mac, time_info);
                    Thread.Sleep(5 * 1000 * 60);
                }

            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }

            Console.ReadLine();
        }


        


        private void GetMACAddress(string[] args)
        {
            NetworkInterface[] nics = NetworkInterface.GetAllNetworkInterfaces();

            string sMacAddress = string.Empty;
            foreach (NetworkInterface adapter in nics)
            {
                if (sMacAddress == String.Empty)// only return MAC Address from first card
                {
                    IPInterfaceProperties properties = adapter.GetIPProperties();
                    sMacAddress = adapter.GetPhysicalAddress().ToString();
                }
            }
            //string strPath = AppDomain.CurrentDomain.BaseDirectory + "Log.txt";
            //System.IO.File.AppendAllLines(strPath, new[] { "Mac address : " + sMacAddress.ToString() });
            addmac = sMacAddress;


        }

        private void GetIPAddress(string[] args)
        {
            NetworkInterface[] adapters = NetworkInterface.GetAllNetworkInterfaces(); //get all network interfaces
            IPAddress[] ips = Dns.GetHostAddresses(Dns.GetHostName()); // get all IP addresses based on the local host name

            foreach (NetworkInterface adapter in adapters) //for each Network interface in addapters
            {
                IPInterfaceProperties properties = adapter.GetIPProperties(); // get the ip properties from the adapter and store them into properties
                foreach (UnicastIPAddressInformation ip in properties.UnicastAddresses) // for each UnicastIPAddressInformation in the IPInterfaceProperties Unicast address( this assocaites the IP address with the correct adapter)
                {
                    //if the operationalStatus of the adapter is up and the ip Address family is in the Internwork
                    if ((adapter.Name == "Ethernet") && (ip.Address.AddressFamily == AddressFamily.InterNetwork)) //test against the name of the adapter you want to get
                    {
                        addip = ip.Address.ToString();

                    }//end if
                }//end inner for, the UnicastIPAddressInformation for
            }

        }


        private void GetHardWare(string[] args)
        {
            ManagementObjectCollection objectList = null;
            ManagementObjectSearcher objectSearcher = new ManagementObjectSearcher("SELECT * FROM Win32_Processor");
            objectList = objectSearcher.Get();
            string getcpu = "";
            foreach (ManagementObject obj in objectList)
            {
                getcpu = obj["Name"].ToString();
            }
            //string strPath = AppDomain.CurrentDomain.BaseDirectory + "Log.txt";
            //System.IO.File.AppendAllLines(strPath, new[] { "CPU : " + getcpu });


            objectSearcher = new ManagementObjectSearcher("SELECT * FROM Win32_BaseBoard");
            objectList = objectSearcher.Get();
            string getmainbord = "";
            foreach (ManagementObject obj in objectList)
            {
                getmainbord = obj["Product"].ToString();
            }
            //string strPath2 = AppDomain.CurrentDomain.BaseDirectory + "Log.txt";
            //System.IO.File.AppendAllLines(strPath2, new[] { "Mainbord : " + getmainbord });


            objectSearcher = new ManagementObjectSearcher("SELECT * FROM Win32_PhysicalMemory");
            objectList = objectSearcher.Get();
            string getram = "";
            foreach (ManagementObject obj in objectList)
            {
                getram = obj["Capacity"].ToString();
            }

            long i = 0;
            i = long.Parse(getram);
            i = i / (1024 * 1024 * 1024);
            string ramgb = i.ToString();
            //string strPath3 = AppDomain.CurrentDomain.BaseDirectory + "Log.txt";
            //System.IO.File.AppendAllLines(strPath3, new[] { "Ram : " + ramgb });

            addcpu = getcpu;
            addmainbord = getmainbord;
            addram = ramgb;
        }

        
        private void GetStartTime(string[] args)
        {
            //string strPath = AppDomain.CurrentDomain.BaseDirectory + "Log.txt";
            //System.IO.File.AppendAllLines(strPath, new[] { "Starting Reset : " + DateTime.Now.ToString() });


            addtime = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");

            addyear = DateTime.Now.ToString("yyyy");
            addmouth = DateTime.Now.ToString("MM");
            addday = DateTime.Now.ToString("dd");
            addhour = DateTime.Now.ToString("HH");
            addminute = DateTime.Now.ToString("mm");
            addsecond = DateTime.Now.ToString("ss");

        }

        private void GetLastTime(string[] args)
        {
            //string strPath = AppDomain.CurrentDomain.BaseDirectory + "Log.txt";
            //System.IO.File.AppendAllLines(strPath, new[] { "Stop Time : " + DateTime.Now.ToString() });
            //addlasttime = this.GetLastSystemShutdown().ToString("yyyy-MM-dd HH:mm:ss");
            addshutdownH = DateTime.Now.ToString("HH");
            addshutdownM = DateTime.Now.ToString("mm");
            addlasttime = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
            

        }

        public DateTime GetLastSystemShutdown()
        {
            string sKey = @"System\CurrentControlSet\Control\Windows";
            Microsoft.Win32.RegistryKey key = Microsoft.Win32.Registry.LocalMachine.OpenSubKey(sKey);

            string sValueName = "ShutdownTime";
            byte[] val = (byte[])key.GetValue(sValueName);
            long valueAsLong = BitConverter.ToInt64(val, 0);
            return DateTime.FromFileTime(valueAsLong);
        }



        protected void DoLoop()
        {

            for (; ; )
            {
                Thread.Sleep(10000);
                this.GetLastTime(null);
               
            }

        }

        public void DoWork()
        {
            while (true)
            {
                
                this.GetLastTime(null);
                Thread.Sleep(20000);

            }
        }



        protected override void OnStart(string[] args)
        {



            ThreadPool.QueueUserWorkItem(new WaitCallback((_) =>
            {
                Thread.Sleep(5 * 1000 * 60);

                this.GetStartTime(null);
                this.GetMACAddress(null);
                this.GetIPAddress(null);
                this.GetHardWare(null);
                this.GetLastTime(null);



                thread = new Thread(RunAsync);
                thread.Start();
            }));


        }

        
        protected override void OnShutdown()
        {

            
        }

        protected override void OnStop()
        {
            thread.Abort();

        }




    }
}
